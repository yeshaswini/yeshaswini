import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  name: string;
  age: number;
  email: string;
  address: {
    street: string,
    city: string,
    state: string
  };
  hobbies: string[];
  constructor(private dataService: DataService ) {  }

  ngOnInit() {
    this.name = 'Yeshu';
    this.age = 23;
    this.email = 'yeshaswinikp@gmail.com';
    this.address = {
      street: '14th main road ',
      city: 'bangalore',
      state: 'Karnataka'
    };
    this.hobbies = ['Listening music', 'watching movie', 'playing'];
  }
  onclick() {
    this.name = 'Yeshaswini';
    this.hobbies.push('New Hobby');
  }
  addHobby(hobby) {
    this.hobbies.unshift(hobby);
    return false;
  }
  deleteHobby(hobby) {
    for (let i = 0; i < this.hobbies.length ; i++) {
      if (this.hobbies[i] === hobby) {
        this.hobbies.splice(i, 1);
      }
    }
  }
}
